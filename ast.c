#include <sys/types.h>

#include <stdio.h>
#include <stdlib.h>

#include "ast.h"

struct ast_type* ast_type_clone(struct ast_type* type)
{
	struct ast_type* result = (struct ast_type*) malloc(sizeof(struct ast_type));
	result->type = type->type;
	switch ( result->type )
	{
	case AST_TYPE_BASIC:
		result->basic = type->basic;
		break;
	case AST_TYPE_FUNCTION:
		result->function.return_type = ast_type_clone(type->function.return_type);
		result->function.param_types = (struct ast_type**)
			malloc(sizeof(struct ast_type*) * result->function.param_types_length);
		for ( size_t i = 0; i < type->function.param_types_length; i++ )
			result->function.param_types[i] = ast_type_clone(type->function.param_types[i]);
		break;
	}
	return result;
}

void ast_type_free(struct ast_type* type)
{
	switch ( type->type )
	{
	case AST_TYPE_BASIC:
		break;
	case AST_TYPE_FUNCTION:
		ast_type_free(type->function.return_type);
		for ( size_t i = 0; i < type->function.param_types_length; i++ )
			ast_type_free(type->function.param_types[i]);
		free(type->function.param_types);
		break;
	}
	free(type);
}

void ast_print_type(FILE* fp, struct ast_type* type, const char* name)
{
	switch ( type->type )
	{
	case AST_TYPE_BASIC:
		switch ( type->basic )
		{
		case AST_TYPE_BASIC_INT: fprintf(fp, "int"); break;
		}
		if ( name )
			fprintf(fp, " %s", name);
		break;
	case AST_TYPE_FUNCTION:
		ast_print_type(fp, type->function.return_type, NULL);
		fprintf(fp, " (*%s)(", name ? name : "");
		for ( size_t i = 0; i < type->function.param_types_length; i++ )
		{
			if ( 0 < i )
				fprintf(fp, ", ");
			ast_print_type(fp, type->function.param_types[i], NULL);
		}
		fprintf(fp, ")");
		break;
	}
}

void ast_indent(FILE* fp, int indent)
{
	while ( 0 < indent-- )
		fprintf(fp, "\t");
}

void ast_print_binop_type(FILE* fp, enum ast_binop_type type)
{
	switch ( type )
	{
	case AST_BINOP_ASSIGNMENT: fprintf(fp, "="); break;
	case AST_BINOP_PLUS: fprintf(fp, "+"); break;
	case AST_BINOP_MINUS: fprintf(fp, "-"); break;
	case AST_BINOP_MULTIPLY: fprintf(fp, "*"); break;
	case AST_BINOP_DIVIDE: fprintf(fp, "/"); break;
	};
}

void ast_print_expression(FILE* fp, struct ast_expression* expr)
{
	switch ( expr->type )
	{
	case AST_EXPRESSION_CONSTANT_INTEGER:
		fprintf(fp, "%s", expr->constant_integer.string);
		break;
	case AST_EXPRESSION_IDENTIFIER:
		fprintf(fp, "%s", expr->identifier.string);
		break;
	case AST_EXPRESSION_BINOP:
		fprintf(fp, "(");
		ast_print_expression(fp, expr->binop.lhs);
		fprintf(fp, " ");
		ast_print_binop_type(fp, expr->binop.type);
		fprintf(fp, " ");
		ast_print_expression(fp, expr->binop.rhs);
		fprintf(fp, ")");
		break;
	case AST_EXPRESSION_FUNCTION_CALL:
		ast_print_expression(fp, expr->function_call.func_expr);
		fprintf(fp, "(");
		for ( size_t i = 0; i < expr->function_call.param_exprs_length; i++ )
		{
			if ( 0 < i )
				fprintf(fp, ", ");
			ast_print_expression(fp, expr->function_call.param_exprs[i]);
		}
		fprintf(fp, ")");
		break;
	case AST_EXPRESSION_LOCAL:
		fprintf(fp, "%s", expr->local.var->name);
		break;
	case AST_EXPRESSION_PARAMETER:
		fprintf(fp, "%s", expr->parameter.param->name);
		break;
	case AST_EXPRESSION_GLOBAL:
		fprintf(fp, "%s", expr->global.var->name);
		break;
	case AST_EXPRESSION_FUNCTION:
		fprintf(fp, "%s", expr->function.func->name);
		break;
	}
}

void ast_print_statement(FILE* fp, int indent, struct ast_statement* stm)
{
	switch ( stm->type )
	{
	case AST_STATEMENT_BLOCK:
		ast_indent(fp, indent);
		fprintf(fp, "{\n");
		for ( size_t i = 0; i < stm->block.stms_length; i++ )
			ast_print_statement(fp, indent + 1, stm->block.stms[i]);
		ast_indent(fp, indent);
		fprintf(fp, "}\n");
		break;
	case AST_STATEMENT_EXPRESSION:
		ast_indent(fp, indent);
		ast_print_expression(fp, stm->expression.expr);
		fprintf(fp, ";\n");
		break;
	case AST_STATEMENT_RETURN:
		ast_indent(fp, indent);
		fprintf(fp, "return ");
		ast_print_expression(fp, stm->return_stm.expr);
		fprintf(fp, ";\n");
		break;
	case AST_STATEMENT_VARIABLE:
		ast_indent(fp, indent);
		ast_print_type(fp, stm->variable.type, stm->variable.name);
		fprintf(fp, ";\n");
		break;
	}
}

void ast_print_function(FILE* fp, struct ast_function* func)
{
	char* proto;
	size_t proto_size;
	FILE* proto_fp = open_memstream(&proto, &proto_size);
	fprintf(proto_fp, "%s(", func->name);
	for ( size_t i = 0; i < func->params_length; i++ )
	{
		if ( 0 < i  )
			fprintf(proto_fp, ", ");
		ast_print_type(proto_fp, func->params[i]->type, func->params[i]->name);
	}
	fprintf(proto_fp, ")");
	fclose(proto_fp);

	ast_print_type(fp, func->return_type, proto);

	free(proto);

	if ( !func->body )
		fprintf(fp, ";");
	fprintf(fp, "\n");
	if ( func->body )
		ast_print_statement(fp, 0, func->body);
}

void ast_print_toplevel(FILE* fp, struct ast_toplevel* tlvl)
{
	switch ( tlvl->type )
	{
	case AST_TOPLEVEL_FUNCTION:
		ast_print_function(fp, tlvl->function.func);
		break;
	case AST_TOPLEVEL_VARIABLE:
		ast_print_type(fp, tlvl->variable.var->type, tlvl->variable.var->name);
	}
}

void ast_print(FILE* fp, struct ast* ast)
{
	for ( size_t i = 0; i < ast->tlvls_length; i++ )
		ast_print_toplevel(fp, ast->tlvls[i]);
}
