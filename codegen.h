#ifndef CODEGEN_H
#define CODEGEN_H

#include <stdbool.h>
#include <stdio.h>

struct ast;

bool codegen(FILE* fp, struct ast* ast, const char* path);

#endif
