#ifndef TOKEN_H
#define TOKEN_H

enum token_type
{
	TOKEN_NONE,
	TOKEN_CONSTANT_INTEGER,
	TOKEN_IDENTIFIER,
	TOKEN_TYPE_INT,
	TOKEN_SEMICOLON,
	TOKEN_ASSIGNMENT,
	TOKEN_BRACE_OPEN,
	TOKEN_BRACE_CLOSE,
	TOKEN_PAREN_OPEN,
	TOKEN_PAREN_CLOSE,
	TOKEN_PLUS,
	TOKEN_DASH,
	TOKEN_ASTERISK,
	TOKEN_SLASH,
	TOKEN_COMMA,
	TOKEN_RETURN,
};

static inline const char* string_of_token_type(enum token_type token_type)
{
	switch ( token_type )
	{
	case TOKEN_NONE: return "none";
	case TOKEN_CONSTANT_INTEGER: return "integer";
	case TOKEN_IDENTIFIER: return "identifier";
	case TOKEN_TYPE_INT: return "int";
	case TOKEN_SEMICOLON: return "semicolon";
	case TOKEN_ASSIGNMENT: return "assignment";
	case TOKEN_BRACE_OPEN: return "brace open";
	case TOKEN_BRACE_CLOSE: return "brace close";
	case TOKEN_PAREN_OPEN: return "paren open";
	case TOKEN_PAREN_CLOSE: return "paren close";
	case TOKEN_PLUS: return "plus";
	case TOKEN_DASH: return "dash";
	case TOKEN_ASTERISK: return "asterisk";
	case TOKEN_SLASH: return "slash";
	case TOKEN_RETURN: return "return";
	case TOKEN_COMMA: return "comma";
	};
	__builtin_unreachable();
}

struct token
{
	enum token_type type;
	char* string;
};

#endif
