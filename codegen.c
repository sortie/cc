#include <sys/types.h>

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ast.h"
#include "codegen.h"

bool codegen_lvalue(FILE* fp,
                    struct ast_expression* expr,
                    bool unused)
{
	assert(expr->is_lvalue);
	switch ( expr->type )
	{
	case AST_EXPRESSION_LOCAL:
		if ( unused )
			break;
		fprintf(fp, "\tleaq\t%zi(%%rbp), %%rax\n", expr->local.var->frame_offset);
		fprintf(fp, "\tpushq\t%%rax\n");
		break;
	case AST_EXPRESSION_PARAMETER:
		if ( unused )
			break;
		fprintf(fp, "\tleaq\t%zi(%%rbp), %%rax\n", expr->parameter.param->frame_offset);
		fprintf(fp, "\tpushq\t%%rax\n");
		break;
	case AST_EXPRESSION_GLOBAL:
		if ( unused )
			break;
		fprintf(fp, "\tpushq\t$%s\n", expr->global.var->name);
		break;
	case AST_EXPRESSION_FUNCTION:
	case AST_EXPRESSION_CONSTANT_INTEGER:
	case AST_EXPRESSION_IDENTIFIER:
	case AST_EXPRESSION_BINOP:
	case AST_EXPRESSION_FUNCTION_CALL:
		__builtin_unreachable();
	}
	return true;
}

bool codegen_expression(FILE* fp,
                        struct ast_expression* expr,
                        bool unused)
{
	switch ( expr->type )
	{
	case AST_EXPRESSION_CONSTANT_INTEGER:
		if ( unused )
			break;
		fprintf(fp, "\tpush\t$%s\n", expr->constant_integer.string);
		break;
	case AST_EXPRESSION_BINOP:
		switch ( expr->binop.type )
		{
		case AST_BINOP_ASSIGNMENT:
			if ( !codegen_lvalue(fp, expr->binop.lhs, false) )
				return false;
			if ( !codegen_expression(fp, expr->binop.rhs, false) )
				return false;
			break;
		default:
			if ( !codegen_expression(fp, expr->binop.lhs, unused) )
				return false;
			// TODO: In the AST_BINOP_DIVIDE case, we might optimize away a
			//       division by zero, which is undefined behavior, but we might
			//       want to trap that.
			if ( !codegen_expression(fp, expr->binop.rhs, unused) )
				return false;
			break;
		}
		if ( expr->binop.type != AST_BINOP_ASSIGNMENT && unused )
			break;
		switch ( expr->binop.type )
		{
		case AST_BINOP_ASSIGNMENT:
			fprintf(fp, "\tpopq\t%%rax\n");
			fprintf(fp, "\tpopq\t%%rcx\n");
			fprintf(fp, "\tmov\t%%eax, (%%rcx)\n");
			if ( unused )
				break;
			fprintf(fp, "\tpush\t%%rax\n");
			break;
		case AST_BINOP_PLUS:
			fprintf(fp, "\tpopq\t%%rax\n");
			fprintf(fp, "\tpopq\t%%rcx\n");
			fprintf(fp, "\taddl\t%%ecx, %%eax\n");
			fprintf(fp, "\tpush\t%%rax\n");
			break;
		case AST_BINOP_MINUS:
			fprintf(fp, "\tpopq\t%%rax\n");
			fprintf(fp, "\tpopq\t%%rcx\n");
			fprintf(fp, "\tsubl\t%%ecx, %%eax\n");
			fprintf(fp, "\tpush\t%%rax\n");
			break;
		case AST_BINOP_MULTIPLY:
			fprintf(fp, "\tpopq\t%%rax\n");
			fprintf(fp, "\tpopq\t%%rcx\n");
			fprintf(fp, "\timull\t%%ecx, %%eax\n");
			fprintf(fp, "\tpush\t%%rax\n");
			break;
		case AST_BINOP_DIVIDE:
			fprintf(fp, "\tpopq\t%%rcx\n");
			fprintf(fp, "\tpopq\t%%rax\n");
			fprintf(fp, "\tcltd\n");
			fprintf(fp, "\tdivl\t%%ecx\n");
			fprintf(fp, "\tpush\t%%rax\n");
			break;
		}
		break;
	case AST_EXPRESSION_FUNCTION_CALL:
		fprintf(fp, "\tpushq\t%%rbx\n");
		fprintf(fp, "\tmovq\t%%rsp, %%rbx\n");
		fprintf(fp, "\tandq\t$-16, %%rsp\n");
		size_t stack_usage = 0;
		stack_usage += 8;
		if ( 6 < expr->function_call.param_exprs_length )
			stack_usage += 8 * (expr->function_call.param_exprs_length - 6);
		if ( stack_usage % 16 )
			fprintf(fp, "\tsubq\t$%zu, %%rsp\n", 16 - (stack_usage % 16));
		if ( !codegen_expression(fp, expr->function_call.func_expr, false) )
			return false;
		size_t stack_params = 0;
		for ( size_t i = expr->function_call.param_exprs_length; i > 0; i-- )
		{
			if ( !codegen_expression(fp, expr->function_call.param_exprs[i-1], false) )
				return false;
			stack_params++;
		}
		size_t regs_index = 0;
		const char* regs[] = { "rdi", "rsi", "rcx", "rdx", "r8", "r9", NULL };
		while ( 0 < stack_params && regs[regs_index] )
		{
			fprintf(fp, "\tpopq\t%%%s\n", regs[regs_index++]);
			stack_params--;
		}
		fprintf(fp, "\tmovq\t%zu(%%rsp), %%rax\n", 8 * stack_params);
		fprintf(fp, "\tcall\t*%%rax\n");
		fprintf(fp, "\tmovq\t%%rbx, %%rsp\n");
		fprintf(fp, "\tpopq\t%%rbx\n");
		if ( !unused )
			fprintf(fp, "\tpushq\t%%rax\n");
		break;
	case AST_EXPRESSION_LOCAL:
		if ( unused )
			break;
		fprintf(fp, "\tmov\t%zi(%%rbp), %%eax\n", expr->local.var->frame_offset);
		fprintf(fp, "\tpushq\t%%rax\n");
		break;
	case AST_EXPRESSION_PARAMETER:
		if ( unused )
			break;
		fprintf(fp, "\tmov\t%zi(%%rbp), %%eax\n", expr->parameter.param->frame_offset);
		fprintf(fp, "\tpushq\t%%rax\n");
		break;
	case AST_EXPRESSION_GLOBAL:
		if ( unused )
			break;
		fprintf(fp, "\tmov\t%s, %%eax\n", expr->global.var->name);
		fprintf(fp, "\tpushq\t%%rax\n");
		break;
	case AST_EXPRESSION_FUNCTION:
		if ( unused )
			break;
		fprintf(fp, "\tpushq\t$%s\n", expr->function.func->name);
		break;
	case AST_EXPRESSION_IDENTIFIER:
		__builtin_unreachable();
	}
	return true;
}

bool codegen_statements_block(FILE* fp,
                              struct ast_statement** stms,
                              size_t stms_length,
                              size_t frame_offset)
{
	size_t original_frame_offset = frame_offset;

	for ( size_t i = 0; i < stms_length; i++ )
	{
		struct ast_statement* stm = stms[i];
		switch ( stm->type )
		{
		case AST_STATEMENT_BLOCK:
			if ( !codegen_statements_block(
					fp, stm->block.stms, stm->block.stms_length,
					frame_offset) )
				return false;
			break;
		case AST_STATEMENT_EXPRESSION:
			if ( !codegen_expression(fp, stm->expression.expr, true) )
				return false;
			break;
		case AST_STATEMENT_RETURN:
			if ( stm->return_stm.expr )
			{
				if ( !codegen_expression(fp, stm->return_stm.expr, false) )
					return false;
				fprintf(fp, "\tpopq\t%%rax\n");
			}
			fprintf(fp, "\tmovq\t%%rbp, %%rsp\n");
			fprintf(fp, "\tpopq\t%%rbp\n");
			fprintf(fp, "\tret\n");
			break;
		case AST_STATEMENT_VARIABLE:
			frame_offset += 8;
			fprintf(fp, "\tsubq\t$8, %%rsp\n");
			stm->variable.frame_offset = -(ssize_t) frame_offset;
			break;
		}
	}

	if ( frame_offset != original_frame_offset )
		fprintf(fp, "\taddq\t$%zu, %%rbp\n", frame_offset - original_frame_offset);

	return true;
}

bool codegen_function(FILE* fp,
                      struct ast_function* func)
{
	if ( !func->body )
		return true;
	fprintf(fp, "\t.text\n");
	fprintf(fp, "\t.globl\t%s\n", func->name);
	fprintf(fp, "\t.type\t%s, @function\n", func->name);
	fprintf(fp, "%s:\n", func->name);
	fprintf(fp, "\tpushq\t%%rbp\n");
	fprintf(fp, "\tmovq\t%%rsp, %%rbp\n");
	size_t parameter_frame_offset = 16;
	size_t frame_offset = 0;
	for ( size_t i = 0; i < func->params_length; i++ )
	{
		const char* reg;
		switch ( i )
		{
		case 0: reg = "rdi"; break;
		case 1: reg = "rsi"; break;
		case 2: reg = "rdx"; break;
		case 3: reg = "rcx"; break;
		case 4: reg = "r8"; break;
		case 5: reg = "r9"; break;
		default: reg = NULL; break;
		};
		if ( reg )
		{
			fprintf(fp, "\tpushq\t%%%s\n", reg);
			frame_offset += 8;
			func->params[i]->frame_offset = -(ssize_t) frame_offset;
		}
		else
		{
			func->params[i]->frame_offset = (ssize_t) parameter_frame_offset;
			parameter_frame_offset += 8;
		}
	}
	if ( !codegen_statements_block(fp, &func->body, 1, frame_offset) )
		return false;
	fprintf(fp, "\t.size\t%s, .-%s\n", func->name, func->name);
	return true;
}

bool codegen_global(FILE* fp,
                    struct ast_global_variable* var)
{
	fprintf(fp, "\t.comm\t%s,4,4\n", var->name);
	return true;
}

bool codegen_toplevels(FILE* fp,
                       struct ast_toplevel** tlvls,
                       size_t tlvls_length)
{
	for ( size_t i = 0; i < tlvls_length; i++ )
	{
		struct ast_toplevel* tlvl = tlvls[i];
		switch ( tlvl->type )
		{
		case AST_TOPLEVEL_FUNCTION:
			if ( !codegen_function(fp, tlvl->function.func) )
				return false;
			break;
		case AST_TOPLEVEL_VARIABLE:
			if ( !codegen_global(fp, tlvl->variable.var) )
				return false;
			break;
		}
	}
	return true;
}

bool codegen(FILE* fp, struct ast* ast, const char* path)
{
	// TODO: Escape the path correctly here.
	fprintf(fp, "\t.file\t\"%s\"\n", path);
	if ( !codegen_toplevels(fp, ast->tlvls, ast->tlvls_length) )
		return false;
	// TODO: Is this what we want to do?
	fprintf(fp, "\t.ident\t\"Sortix CC\"\n");
	fprintf(fp, "\t.section\t.note.GNU-stack,\"\",@progbits\n");
	return true;
}
