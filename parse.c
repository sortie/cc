#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ast.h"
#include "token.h"
#include "parse.h"

struct parse_operator
{
	enum token_type token_type;
	enum ast_binop_type binop_type;
	bool binary;
	bool right_to_left;
};

struct parse_operator parse_operators[] =
{
	{ TOKEN_ASSIGNMENT, AST_BINOP_ASSIGNMENT, .binary = true, .right_to_left = true },
	{ TOKEN_PLUS, AST_BINOP_PLUS, .binary = true, .right_to_left = false },
	{ TOKEN_DASH, AST_BINOP_MINUS, .binary = true, .right_to_left = false },
	{ TOKEN_ASTERISK, AST_BINOP_MULTIPLY, .binary = true, .right_to_left = false },
	{ TOKEN_SLASH, AST_BINOP_DIVIDE, .binary = true, .right_to_left = false },
};

bool parse_next_is_token(size_t* token_index,
                         struct token** tokens,
                         size_t tokens_length,
                         enum token_type type);

bool try_parse_end(size_t* token_index,
                       struct token** tokens,
                       size_t tokens_length)
{
	(void) tokens;
	return *token_index == tokens_length;
}

bool try_parse_token(size_t* token_index,
                     struct token** tokens,
                     size_t tokens_length,
                     enum token_type type)
{
	if ( try_parse_end(token_index, tokens, tokens_length) )
		return false;
	if ( tokens[*token_index]->type != type )
		return false;
	return (*token_index)++, true;
}

bool try_parse_type(size_t* token_index,
                    struct token** tokens,
                    size_t tokens_length)
{
	return try_parse_token(token_index, tokens, tokens_length, TOKEN_TYPE_INT);
}

bool try_parse_function(size_t* token_index,
                        struct token** tokens,
                        size_t tokens_length)
{
	if ( !try_parse_type(token_index, tokens, tokens_length) )
		return false;
	if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER) )
		return false;
	if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_PAREN_OPEN) )
		return false;

	size_t params_length = 0;
	while ( !parse_next_is_token(token_index, tokens, tokens_length, TOKEN_PAREN_CLOSE) )
	{
		if ( 0 < params_length )
		{
			if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_COMMA) )
				return false;
		}
		if ( !try_parse_type(token_index, tokens, tokens_length) )
			return false;
		if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER) )
			return false;
		params_length++;
	}

	if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_PAREN_CLOSE) )
		return false;

	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return true;
	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_BRACE_OPEN) )
		return true;
	return false;
}

bool try_parse_global_variable(size_t* token_index,
                               struct token** tokens,
                               size_t tokens_length)
{
	if ( !try_parse_type(token_index, tokens, tokens_length) )
		return false;
	if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER) )
		return false;
	if ( !parse_next_is_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return false;
	return true;
}

bool try_parse_statement_variable(size_t* token_index,
                                  struct token** tokens,
                                  size_t tokens_length)
{
	if ( !try_parse_type(token_index, tokens, tokens_length) )
		return false;
	if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER) )
		return false;
	if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return false;
	return true;
}

bool parse_next_is_statement_variable(size_t* token_index,
                                      struct token** tokens,
                                      size_t tokens_length)
{
	size_t original_token_index = *token_index;
	bool result = try_parse_statement_variable(token_index, tokens, tokens_length);
	*token_index = original_token_index;
	return result;
}

bool parse_next_is_token(size_t* token_index,
                         struct token** tokens,
                         size_t tokens_length,
                         enum token_type type)
{
	size_t original_token_index = *token_index;
	bool result = try_parse_token(token_index, tokens, tokens_length, type);
	*token_index = original_token_index;
	return result;
}

bool parse_next_is_function(size_t* token_index,
                            struct token** tokens,
                            size_t tokens_length)
{
	size_t original_token_index = *token_index;
	bool result = try_parse_function(token_index, tokens, tokens_length);
	*token_index = original_token_index;
	return result;
}

bool parse_next_is_global_variable(size_t* token_index,
                                   struct token** tokens,
                                   size_t tokens_length)
{
	size_t original_token_index = *token_index;
	bool result = try_parse_global_variable(token_index, tokens, tokens_length);
	*token_index = original_token_index;
	return result;
}

struct token* parse_token(size_t* token_index,
                          struct token** tokens,
                          size_t tokens_length,
                          enum token_type type)
{
	if ( try_parse_end(token_index, tokens, tokens_length) ||
	     tokens[*token_index]->type != type )
	{
		fprintf(stderr, "Expected %s at token %zu\n", string_of_token_type(type), *token_index);
		return NULL;
	}
	return tokens[(*token_index)++];
}

struct ast_type* parse_make_basic_type(enum ast_type_basic basic)
{
	struct ast_type* result = (struct ast_type*) malloc(sizeof(struct ast_type));
	result->type = AST_TYPE_BASIC;
	result->basic = basic;
	return result;
}

struct ast_type* parse_type(size_t* token_index,
                            struct token** tokens,
                            size_t tokens_length)
{
	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_TYPE_INT) )
		return NULL;
	return parse_make_basic_type(AST_TYPE_BASIC_INT);
}

struct ast_expression* parse_expression(size_t* token_index,
                                        struct token** tokens,
                                        size_t tokens_length);

struct ast_expression* parse_expr_constant(size_t* token_index,
                                           struct token** tokens,
                                           size_t tokens_length)
{
	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_CONSTANT_INTEGER) )
	{
		struct token* value_token;

		if ( !(value_token = parse_token(token_index, tokens, tokens_length, TOKEN_CONSTANT_INTEGER)) )
			return NULL;

		struct ast_expression* expr = (struct ast_expression*) malloc(sizeof(struct ast_expression));
		expr->type = AST_EXPRESSION_CONSTANT_INTEGER;
		expr->constant_integer.string = strdup(value_token->string);
		return expr;
	}
	return NULL;
}

struct ast_expression* parse_expr_identifier(size_t* token_index,
                                             struct token** tokens,
                                             size_t tokens_length)
{
	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER) )
	{
		struct token* name_token;

		if ( !(name_token = parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER)) )
			return NULL;

		struct ast_expression* expr = (struct ast_expression*) malloc(sizeof(struct ast_expression));
		expr->type = AST_EXPRESSION_IDENTIFIER;
		expr->identifier.string = strdup(name_token->string);
		return expr;
	}
	return parse_expr_constant(token_index, tokens, tokens_length);
}

struct ast_expression* parse_expr_function_call(size_t* token_index,
                                                struct token** tokens,
                                                size_t tokens_length)
{
	struct ast_expression* expr;
	if ( !(expr = parse_expr_identifier(token_index, tokens, tokens_length)) )
		return NULL;
	size_t original_token_index = *token_index;
	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_PAREN_OPEN) )
	{
		if ( !(parse_token(token_index, tokens, tokens_length, TOKEN_PAREN_OPEN)) )
			return NULL;
		struct ast_expression** params = NULL;
		size_t params_used = 0;
		size_t params_length = 0;
		while ( !parse_next_is_token(token_index, tokens, tokens_length, TOKEN_PAREN_CLOSE) )
		{
			if ( 0 < params_used )
			{
				if ( !(parse_token(token_index, tokens, tokens_length, TOKEN_COMMA)) )
					return NULL;
			}
			if ( params_used == params_length )
			{
				params_length = params_length ? 2 * params_length : 16;
				params = (struct ast_expression**) realloc(params, sizeof(struct ast_expression*) * params_length);
			}
			struct ast_expression* param;
			if ( !(param = parse_expression(token_index, tokens, tokens_length)) )
				return NULL;
			params[params_used++] = param;
		}
		if ( !(parse_token(token_index, tokens, tokens_length, TOKEN_PAREN_CLOSE)) )
			return NULL;
		struct ast_expression* call = (struct ast_expression*) malloc(sizeof(struct ast_expression));
		call->type = AST_EXPRESSION_FUNCTION_CALL;
		call->function_call.func_expr = expr;
		call->function_call.param_exprs = params;
		call->function_call.param_exprs_length = params_used;
		return call;
	}
	*token_index = original_token_index;
	return expr;
}

struct ast_expression* parse_expression_operator(size_t* token_index,
                                                 struct token** tokens,
                                                 size_t tokens_length,
                                                 struct parse_operator* operators,
                                                 size_t operators_length)
{
	while ( true )
	{
		if ( operators_length == 0 )
			return parse_expr_function_call(token_index, tokens, tokens_length);

		struct parse_operator* operator = operators;

		bool found = false;
		size_t found_at_pos = 0;
		size_t parenthesis_level = 0;
		for ( size_t pos = *token_index; true; pos++ )
		{
			if ( pos == tokens_length )
				break;
			if ( tokens[pos]->type == TOKEN_SEMICOLON )
				break;
			if ( tokens[pos]->type == TOKEN_PAREN_OPEN )
			{
				parenthesis_level++;
				continue;
			}
			if ( tokens[pos]->type == TOKEN_PAREN_CLOSE )
			{
				if ( parenthesis_level == 0 )
					break;
				parenthesis_level--;
				continue;
			}
			if ( 0 < parenthesis_level )
				continue;
			if ( tokens[pos]->type == TOKEN_COMMA )
				break;
			if ( tokens[pos]->type != operator->token_type )
				continue;
			found = true;
			found_at_pos = pos;
			if ( operator->right_to_left )
				break;
		}

		if ( 0 < parenthesis_level )
		{
			fprintf(stderr, "Expected %zu )'s before token %zu\n", parenthesis_level, *token_index);
			return NULL;
		}

		if ( !found )
		{
			operators++;
			operators_length--;
			continue;
		}

		size_t pos = found_at_pos;

		struct ast_expression* lhs;
		struct ast_expression* rhs;

		if ( operator->right_to_left )
		{
			if ( !(lhs = parse_expression_operator(
				token_index, tokens, pos,
				operators + 1, operators_length - 1)) )
				return NULL;
			if ( *token_index != pos )
			{
				fprintf(stderr, "Expression parse error before token %zu\n", *token_index);
				return NULL;
			}
			(*token_index)++;
			if ( !(rhs = parse_expression_operator(
				token_index, tokens, tokens_length,
				operators, operators_length)) )
				return NULL;
		}
		else
		{
			if ( !(lhs = parse_expression_operator(
				token_index, tokens, pos,
				operators, operators_length)) )
				return NULL;
			if ( *token_index != pos )
			{
				fprintf(stderr, "Expression parse error before token %zu\n", *token_index);
				return NULL;
			}
			(*token_index)++;
			if ( !(rhs = parse_expression_operator(
				token_index, tokens, tokens_length,
				operators + 1, operators_length - 1)) )
				return NULL;
		}

		struct ast_expression* expr = (struct ast_expression*) malloc(sizeof(struct ast_expression));
		expr->type = AST_EXPRESSION_BINOP;
		expr->binop.type = operator->binop_type;
		expr->binop.lhs = lhs;
		expr->binop.rhs = rhs;
		return expr;
	}
}

struct ast_expression* parse_expression(size_t* token_index,
                                        struct token** tokens,
                                        size_t tokens_length)
{
	return parse_expression_operator(
		token_index, tokens, tokens_length,
		parse_operators, sizeof(parse_operators) / sizeof(parse_operators[0]));
}

struct ast_statement* parse_statement_expression(size_t* token_index,
                                                 struct token** tokens,
                                                 size_t tokens_length)
{
	struct ast_expression* expr;

	if ( !(expr = parse_expression(token_index, tokens, tokens_length)) )
		return false;
	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return false;

	struct ast_statement* stm = (struct ast_statement*) malloc(sizeof(struct ast_statement));
	stm->type = AST_STATEMENT_EXPRESSION;
	stm->expression.expr = expr;
	return stm;
}

struct ast_statement* parse_statement_return(size_t* token_index,
                                             struct token** tokens,
                                             size_t tokens_length)
{
	struct ast_expression* expr;

	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_RETURN) )
		return false;
	if ( !(expr = parse_expression(token_index, tokens, tokens_length)) )
		return false;
	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return false;

	struct ast_statement* stm = (struct ast_statement*) malloc(sizeof(struct ast_statement));
	stm->type = AST_STATEMENT_RETURN;
	stm->return_stm.expr = expr;
	return stm;
}

struct ast_statement* parse_statement_variable(size_t* token_index,
                                               struct token** tokens,
                                               size_t tokens_length)
{
	struct ast_type* type;
	struct token* name_token;

	if ( !(type = parse_type(token_index, tokens, tokens_length)) )
		return NULL;
	if ( !(name_token = parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER)) )
		return NULL;
	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return NULL;

	struct ast_statement* stm = (struct ast_statement*) malloc(sizeof(struct ast_statement));
	stm->type = AST_STATEMENT_VARIABLE;
	stm->variable.type = type;
	stm->variable.name = strdup(name_token->string);
	return stm;
}

struct ast_statement* parse_statement(size_t* token_index,
                                      struct token** tokens,
                                      size_t tokens_length)
{
	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_RETURN) )
		return parse_statement_return(token_index, tokens, tokens_length);
	if ( parse_next_is_statement_variable(token_index, tokens, tokens_length) )
		return parse_statement_variable(token_index, tokens, tokens_length);
	return parse_statement_expression(token_index, tokens, tokens_length);
}

struct ast_statement* parse_block_statement(size_t* token_index,
                                            struct token** tokens,
                                            size_t tokens_length)
{
	size_t stms_used = 0;
	size_t stms_length = 0;
	struct ast_statement** stms = NULL;

	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_BRACE_OPEN) )
		return false;
	while ( true )
	{
		if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_BRACE_CLOSE) )
		{
			if ( !parse_token(token_index, tokens, tokens_length, TOKEN_BRACE_CLOSE) )
				return NULL;
			break;
		}

		if ( stms_used == stms_length )
		{
			stms_length = stms_length ? 2 * stms_length : 16;
			stms = (struct ast_statement**) realloc(stms, sizeof(struct ast_statement*) * stms_length);
		}

		struct ast_statement* stm;
		if ( !(stm = parse_statement(token_index, tokens, tokens_length)) )
			return NULL;

		stms[stms_used++] = stm;
	}

	struct ast_statement* stm = (struct ast_statement*) malloc(sizeof(struct ast_statement));
	stm->type = AST_STATEMENT_BLOCK;
	stm->block.stms = stms;
	stm->block.stms_length = stms_used;
	return stm;
}

struct ast_function* parse_function(size_t* token_index,
                                    struct token** tokens,
                                    size_t tokens_length)
{
	struct ast_type* type;
	struct token* name_token;
	struct ast_statement* body = NULL;
	struct ast_parameter** params = NULL;
	size_t params_used = 0;
	size_t params_length = 0;

	if ( !(type = parse_type(token_index, tokens, tokens_length)) )
		return NULL;
	if ( !(name_token = parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER)) )
		return NULL;
	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_PAREN_OPEN) )
		return NULL;

	while ( !parse_next_is_token(token_index, tokens, tokens_length, TOKEN_PAREN_CLOSE) )
	{
		if ( 0 < params_length )
		{
			if ( !try_parse_token(token_index, tokens, tokens_length, TOKEN_COMMA) )
				return NULL;
		}

		struct ast_type* type;
		struct token* name_token;

		if ( !(type = parse_type(token_index, tokens, tokens_length)) )
			return NULL;
		if ( !(name_token = parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER)) )
			return NULL;

		if ( params_used == params_length )
		{
			params_length = params_length ? 2 * params_length : 16;
			params = (struct ast_parameter**) realloc(params, sizeof(struct ast_parameter*) * params_length);
		}

		struct ast_parameter* param = (struct ast_parameter*) malloc(sizeof(struct ast_parameter));
		param->type = type;
		param->name = strdup(name_token->string);
		params[params_used++] = param;
	}

	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_PAREN_CLOSE) )
		return NULL;

	if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
	{
		if ( !parse_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
			return NULL;
	}
	else if ( parse_next_is_token(token_index, tokens, tokens_length, TOKEN_BRACE_OPEN) )
	{
		if ( !(body = parse_block_statement(token_index, tokens, tokens_length)) )
			return NULL;
	}
	else
	{
		fprintf(stderr, "Expected semicolon or function body at token %zu\n", *token_index);
		return NULL;
	}

	struct ast_function* func = (struct ast_function*) malloc(sizeof(struct ast_function));
	func->return_type = type;
	func->name = strdup(name_token->string);
	func->body = body;
	func->params = params;
	func->params_length = params_used;
	return func;
}

struct ast_global_variable* parse_global_variable(size_t* token_index,
                                                  struct token** tokens,
                                                  size_t tokens_length)
{
	struct ast_type* type;
	struct token* name_token;

	if ( !(type = parse_type(token_index, tokens, tokens_length)) )
		return NULL;
	if ( !(name_token = parse_token(token_index, tokens, tokens_length, TOKEN_IDENTIFIER)) )
		return NULL;
	if ( !parse_token(token_index, tokens, tokens_length, TOKEN_SEMICOLON) )
		return NULL;

	struct ast_global_variable* var = (struct ast_global_variable*) malloc(sizeof(struct ast_global_variable));
	var->type = type;
	var->name = strdup(name_token->string);
	return var;
}

struct ast_toplevel* parse_toplevel(size_t* token_index,
                                    struct token** tokens,
                                    size_t tokens_length)
{
	if ( parse_next_is_function(token_index, tokens, tokens_length) )
	{
		struct ast_function* func;

		if ( !(func = parse_function(token_index, tokens, tokens_length)) )
			return NULL;

		struct ast_toplevel* tlvl = (struct ast_toplevel*) malloc(sizeof(struct ast_toplevel));
		tlvl->type = AST_TOPLEVEL_FUNCTION;
		tlvl->function.func = func;
		return tlvl;
	}

	if ( parse_next_is_global_variable(token_index, tokens, tokens_length) )
	{
		struct ast_global_variable* var;

		if ( !(var = parse_global_variable(token_index, tokens, tokens_length)) )
			return NULL;

		struct ast_toplevel* tlvl = (struct ast_toplevel*) malloc(sizeof(struct ast_toplevel));
		tlvl->type = AST_TOPLEVEL_VARIABLE;
		tlvl->variable.var = var;
		return tlvl;
	}

	fprintf(stderr, "Expected toplevel at token %zu\n", *token_index);
	return NULL;
}

struct ast* parse(struct token** tokens, size_t tokens_length)
{
	size_t tlvls_used = 0;
	size_t tlvls_length = 0;
	struct ast_toplevel** tlvls = NULL;

	size_t token_index = 0;
	while ( !try_parse_end(&token_index, tokens, tokens_length) )
	{
		struct ast_toplevel* tlvl;

		if ( tlvls_used == tlvls_length )
		{
			tlvls_length = tlvls_length ? 2 * tlvls_length : 16;
			tlvls = (struct ast_toplevel**) realloc(tlvls, sizeof(struct ast_toplevel*) * tlvls_length);
		}

		if ( !(tlvl = parse_toplevel(&token_index, tokens, tokens_length)) )
			return NULL;

		tlvls[tlvls_used++] = tlvl;
	}

	struct ast* ast = (struct ast*) malloc(sizeof(struct ast));
	ast->tlvls = tlvls;
	ast->tlvls_length = tlvls_used;
	return ast;
}
