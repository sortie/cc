#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "ast.h"
#include "check.h"

enum scope_entry_type
{
	SCOPE_ENTRY_LOCAL,
	SCOPE_ENTRY_PARAMETER,
	SCOPE_ENTRY_GLOBAL,
	SCOPE_ENTRY_FUNCTION,
};

struct scope_entry_local
{
	struct ast_statement_variable* var;
};

struct scope_entry_parameter
{
	struct ast_parameter* param;
};

struct scope_entry_global
{
	struct ast_global_variable* var;
};

struct scope_entry_function
{
	struct ast_function* func;
};

struct scope_entry
{
	struct scope_entry* next;
	char* name;
	enum scope_entry_type type;
	union
	{
		struct scope_entry_local local;
		struct scope_entry_parameter parameter;
		struct scope_entry_global global;
		struct scope_entry_function function;
	};
};

struct scope_entry* scope_lookup(struct scope_entry* scope, const char* name)
{
	while ( scope )
	{
		if ( !strcmp(scope->name, name) )
			return scope;
		scope = scope->next;
	}
	return NULL;
}

struct scope_entry* scope_push_local(struct scope_entry* scope,
                                     struct ast_statement_variable* variable)
{
	struct scope_entry* new_scope = (struct scope_entry*) calloc(sizeof(struct scope_entry), 1);
	new_scope->next = scope;
	new_scope->name = strdup(variable->name);
	new_scope->type = SCOPE_ENTRY_LOCAL;
	new_scope->local.var = variable;
	return new_scope;
}

struct scope_entry* scope_push_parameter(struct scope_entry* scope,
                                         struct ast_parameter* parameter)
{
	struct scope_entry* new_scope = (struct scope_entry*) calloc(sizeof(struct scope_entry), 1);
	new_scope->next = scope;
	new_scope->name = strdup(parameter->name);
	new_scope->type = SCOPE_ENTRY_PARAMETER;
	new_scope->parameter.param = parameter;
	return new_scope;
}

struct scope_entry* scope_push_global(struct scope_entry* scope,
                                      struct ast_global_variable* variable)
{
	struct scope_entry* new_scope = (struct scope_entry*) calloc(sizeof(struct scope_entry), 1);
	new_scope->next = scope;
	new_scope->name = strdup(variable->name);
	new_scope->type = SCOPE_ENTRY_GLOBAL;
	new_scope->global.var = variable;
	return new_scope;
}

struct scope_entry* scope_push_function(struct scope_entry* scope,
                                        struct ast_function* function)
{
	struct scope_entry* new_scope = (struct scope_entry*) calloc(sizeof(struct scope_entry), 1);
	new_scope->next = scope;
	new_scope->name = strdup(function->name);
	new_scope->type = SCOPE_ENTRY_FUNCTION;
	new_scope->function.func = function;
	return new_scope;
}

void scope_pop(struct scope_entry** scope_ptr)
{
	struct scope_entry* scope = *scope_ptr;
	*scope_ptr = scope->next;
	free(scope->name);
	free(scope);
}

struct ast_type* check_make_basic_type(enum ast_type_basic basic)
{
	struct ast_type* result = (struct ast_type*) malloc(sizeof(struct ast_type));
	result->type = AST_TYPE_BASIC;
	result->basic = basic;
	return result;
}

struct ast_type* check_make_function_type(struct ast_function* func)
{
	struct ast_type* result = (struct ast_type*) malloc(sizeof(struct ast_type));
	result->type = AST_TYPE_FUNCTION;
	result->function.return_type = ast_type_clone(func->return_type);
	result->function.param_types_length = func->params_length;
	result->function.param_types = (struct ast_type**)
		malloc(sizeof(struct ast_type*) * result->function.param_types_length);
	for ( size_t i = 0; i < result->function.param_types_length; i++ )
		result->function.param_types[i] = ast_type_clone(func->params[i]->type);
	return result;
}

bool is_compatible_type_assignment(struct ast_type* lhs,
                                   struct ast_type* rhs);

struct ast_type* check_compatible_type_assignment(struct ast_type* lhs,
                                                  struct ast_type* rhs)
{
	switch ( rhs->type )
	{
	case AST_TYPE_BASIC:
		if ( lhs->type != AST_TYPE_BASIC )
			return NULL;
		if ( lhs->basic != rhs->basic )
			return NULL;
		break;
	case AST_TYPE_FUNCTION:
		if ( lhs->type != AST_TYPE_FUNCTION )
			return NULL;
		if ( lhs->function.param_types_length !=
		     rhs->function.param_types_length )
			return NULL;
		for ( size_t i = 0; i < rhs->function.param_types_length; i++ )
		{
			if ( !is_compatible_type_assignment(
					lhs->function.param_types[i],
					rhs->function.param_types[i]) )
				return NULL;
		}
		if ( !is_compatible_type_assignment(
				lhs->function.return_type,
				rhs->function.return_type) )
			return NULL;
		break;
	}

	struct ast_type* result = ast_type_clone(rhs);
	assert(result);
	return result;
}

bool is_compatible_type_assignment(struct ast_type* lhs,
                                   struct ast_type* rhs)
{
	struct ast_type* type;
	if ( !(type = check_compatible_type_assignment(lhs, rhs)) )
		return false;
	ast_type_free(type);
	return true;
}

struct ast_type* check_compatible_type_binary(struct ast_type* lhs,
                                              struct ast_type* rhs,
                                              enum ast_binop_type binop)
{
	if ( binop == AST_BINOP_ASSIGNMENT )
		return check_compatible_type_assignment(lhs, rhs);
	if ( lhs->type == AST_TYPE_FUNCTION )
		return NULL;
	if ( rhs->type == AST_TYPE_FUNCTION )
		return NULL;
	return check_make_basic_type(AST_TYPE_BASIC_INT);
}

bool check_expression(struct ast_expression* expr,
                      struct scope_entry* scope)
{
	switch ( expr->type )
	{
	case AST_EXPRESSION_CONSTANT_INTEGER:
		expr->expression_type = check_make_basic_type(AST_TYPE_BASIC_INT);
		expr->is_lvalue = false;
		break;
	case AST_EXPRESSION_IDENTIFIER:
	{
		struct scope_entry* entry;
		if ( !(entry = scope_lookup(scope, expr->identifier.string)) )
		{
			fprintf(stderr, "No ‘%s’ in scope\n", expr->identifier.string);
			return false;
		}
		switch ( entry->type )
		{
		case SCOPE_ENTRY_LOCAL:
			free(expr->identifier.string);
			expr->type = AST_EXPRESSION_LOCAL;
			expr->local.var = entry->local.var;
			expr->expression_type = ast_type_clone(expr->local.var->type);
			expr->is_lvalue = true;
			break;
		case SCOPE_ENTRY_PARAMETER:
			free(expr->identifier.string);
			expr->type = AST_EXPRESSION_PARAMETER;
			expr->parameter.param = entry->parameter.param;
			expr->expression_type = ast_type_clone(expr->parameter.param->type);
			expr->is_lvalue = true;
			break;
		case SCOPE_ENTRY_GLOBAL:
			free(expr->identifier.string);
			expr->type = AST_EXPRESSION_GLOBAL;
			expr->global.var = entry->global.var;
			expr->expression_type = ast_type_clone(expr->global.var->type);
			expr->is_lvalue = true;
			break;
		case SCOPE_ENTRY_FUNCTION:
			free(expr->identifier.string);
			expr->type = AST_EXPRESSION_FUNCTION;
			expr->function.func = entry->function.func;
			expr->expression_type = check_make_function_type(entry->function.func);
			expr->is_lvalue = false;
			break;
		}
		break;
	}
	case AST_EXPRESSION_BINOP:
		if ( !check_expression(expr->binop.lhs, scope) )
			return false;
		if ( !check_expression(expr->binop.rhs, scope) )
			return false;
		if ( !(expr->expression_type = check_compatible_type_binary(
				expr->binop.lhs->expression_type,
				expr->binop.rhs->expression_type,
				expr->binop.type)) )
		{
			fprintf(stderr, "Binary operator operands type mismatch\n");
			return false;
		}
		switch ( expr->binop.type )
		{
		case AST_BINOP_ASSIGNMENT:
			if ( !expr->binop.lhs->is_lvalue )
			{
				fprintf(stderr, "Assignment to non-lvalue\n");
				return false;
			}
			break;
		default:
			break;
		}
		expr->is_lvalue = false;
		break;
	case AST_EXPRESSION_FUNCTION_CALL:
	{
		if ( !check_expression(expr->function_call.func_expr, scope) )
			return false;
		for ( size_t i = 0; i < expr->function_call.param_exprs_length; i++ )
		{
			if ( !check_expression(expr->function_call.param_exprs[i], scope) )
				return false;
		}
		struct ast_type* func_type = expr->function_call.func_expr->expression_type;
		if ( func_type->type != AST_TYPE_FUNCTION )
		{
			fprintf(stderr, "Invocation of non-function\n");
			return false;
		}
		if ( expr->function_call.param_exprs_length !=
		     func_type->function.param_types_length )
		{
			fprintf(stderr, "Function was passed %zu parameters instead of %zu\n",
				expr->function_call.param_exprs_length,
				func_type->function.param_types_length);
			return false;
		}
		for ( size_t i = 0; i < expr->function_call.param_exprs_length; i++ )
		{
			if ( !is_compatible_type_assignment(
					func_type->function.param_types[i],
					expr->function_call.param_exprs[i]->expression_type ))
			{
				fprintf(stderr, "Parameter %zu has wrong type\n", i);
				return false;
			}
		}
		expr->expression_type = ast_type_clone(func_type->function.return_type);
		expr->is_lvalue = false;
	} break;
	case AST_EXPRESSION_LOCAL:
	case AST_EXPRESSION_PARAMETER:
	case AST_EXPRESSION_GLOBAL:
	case AST_EXPRESSION_FUNCTION:
		 __builtin_unreachable();
	}
	return true;
}

bool check_statements_block(struct ast_statement** stms,
                            size_t stms_length,
                            struct scope_entry* scope,
                            struct ast_type* return_type)
{
	struct scope_entry* original_scope = scope;

	for ( size_t i = 0; i < stms_length; i++ )
	{
		struct ast_statement* stm = stms[i];
		switch ( stm->type )
		{
		case AST_STATEMENT_BLOCK:
			if ( !check_statements_block(
					stm->block.stms, stm->block.stms_length,
					scope, return_type) )
				return false;
			break;
		case AST_STATEMENT_EXPRESSION:
			if ( !check_expression( stm->expression.expr, scope) )
				return false;
			break;
		case AST_STATEMENT_RETURN:
			if ( stm->return_stm.expr )
			{
				if ( !check_expression(stm->return_stm.expr, scope) )
					return false;
				if ( !is_compatible_type_assignment(
						stm->return_stm.expr->expression_type,
						return_type ))
				{
					fprintf(stderr, "Return statement expression has wrong type\n");
					return false;
				}
			}
			else
			{
				fprintf(stderr, "Return statement has no expression\n");
				return false;
			}
			break;
		case AST_STATEMENT_VARIABLE:
			scope = scope_push_local(scope, &stm->variable);
			// TODO: Error if shadowing a variable inside this block scope.
			break;
		}
	}

	while ( scope != original_scope )
		scope_pop(&scope);

	return true;
}

bool check_function(struct ast_function* func, struct scope_entry* scope)
{
	if ( !func->body )
		return true;

	struct scope_entry* original_scope = scope;
	for ( size_t i = 0; i < func->params_length; i++ )
		scope = scope_push_parameter(scope, func->params[i]);

	if ( !check_statements_block(&func->body, 1, scope, func->return_type) )
		return false;

	while ( scope != original_scope )
		scope_pop(&scope);

	return true;
}

bool check_toplevels(struct ast_toplevel** tlvls,
                     size_t tlvls_length)
{
	struct scope_entry* scope = NULL;
	struct scope_entry* original_scope = scope;
	for ( size_t i = 0; i < tlvls_length; i++ )
	{
		struct ast_toplevel* tlvl = tlvls[i];
		switch ( tlvl->type )
		{
		case AST_TOPLEVEL_FUNCTION:
			scope = scope_push_function(scope, tlvl->function.func);
			if ( !check_function(tlvl->function.func, scope) )
				return false;
			break;
		case AST_TOPLEVEL_VARIABLE:
			scope = scope_push_global(scope, tlvl->variable.var);
			break;
		}
	}
	while ( scope != original_scope )
		scope_pop(&scope);
	return true;
}

bool check(struct ast* ast)
{
	if ( !check_toplevels(ast->tlvls, ast->tlvls_length) )
		return false;
	return true;
}
