#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "token.h"
#include "tokenize.h"

bool is_identifier_character(char c)
{
	return ('a' <= c && c <= 'z') ||
	       ('A' <= c && c <= 'Z') ||
	       ('0' <= c && c <= '9') ||
	       c == '_';
}

bool check_if_identifier_is(const char* token, size_t length, const char* check)
{
	return strlen(check) == length &&
	       !strncmp(token, check, length);
}

enum token_type classify_identifier_token(const char* token, size_t length)
{
	if ( check_if_identifier_is(token, length, "int") )
		return TOKEN_TYPE_INT;
	if ( check_if_identifier_is(token, length, "return") )
		return TOKEN_RETURN;
	return TOKEN_IDENTIFIER;
}

bool recognize_token(size_t* token_length_ptr,
                     enum token_type* token_type_ptr,
                     const char* str)
{
	size_t token_length = 0;

	if ( isspace((unsigned char) str[token_length]) )
	{
		token_length++;
		while ( isspace((unsigned char) str[token_length]) )
			token_length++;
		*token_type_ptr = TOKEN_NONE;
		*token_length_ptr = token_length;
		return true;
	}

	struct
	{
		const char* string;
		enum token_type type;
	} trivials[] =
	{
		{ "{", TOKEN_BRACE_OPEN },
		{ "}", TOKEN_BRACE_CLOSE },
		{ "(", TOKEN_PAREN_OPEN },
		{ ")", TOKEN_PAREN_CLOSE },
		{ "=", TOKEN_ASSIGNMENT },
		{ ";", TOKEN_SEMICOLON },
		{ "+", TOKEN_PLUS },
		{ "-", TOKEN_DASH },
		{ "*", TOKEN_ASTERISK },
		{ "/", TOKEN_SLASH },
		{ ",", TOKEN_COMMA },
	};

	for ( size_t i = 0; i < sizeof(trivials) / sizeof(trivials[0]); i++ )
	{
		const char* trivial = trivials[i].string;
		size_t trivial_length = strlen(trivial);
		if ( strncmp(str, trivial, trivial_length) != 0 )
			continue;
		*token_type_ptr = trivials[i].type;
		*token_length_ptr = trivial_length;
		return true;
	}

	if ( str[token_length] == '{' )
	{
		token_length++;
		*token_type_ptr = TOKEN_BRACE_OPEN;
		*token_length_ptr = token_length;
		return true;
	}

	if ( '0' <= str[token_length] && str[token_length] <= '9' )
	{
		token_length++;
		while ( '0' <= str[token_length] && str[token_length] <= '9' )
			token_length++;
		*token_type_ptr = TOKEN_CONSTANT_INTEGER;
		*token_length_ptr = token_length;
		return true;
	}

	if ( is_identifier_character(str[token_length]) )
	{
		token_length++;
		while ( is_identifier_character(str[token_length]) )
			token_length++;
		*token_type_ptr = classify_identifier_token(str, token_length);
		*token_length_ptr = token_length;
		return true;
	}

	return false;
}

bool tokenize(struct token*** tokens_ptr,
              size_t* tokens_used_ptr,
              const char* input)
{
	size_t tokens_used = 0;
	size_t tokens_length = 0;
	struct token** tokens = NULL;

	size_t input_offset = 0;
	while ( input[input_offset] )
	{
		const char* token_string = input + input_offset;
		size_t token_length;
		enum token_type token_type;
		if ( !recognize_token(&token_length, &token_type, token_string) )
		{
			fprintf(stderr, "Parse error at “%s”\n", token_string);
			return false;
		}
		input_offset += token_length;
		if ( token_type == TOKEN_NONE )
			continue;
		if ( tokens_used == tokens_length )
		{
			tokens_length = tokens_length ? 2 * tokens_length : 16;
			tokens = (struct token**) realloc(tokens, sizeof(struct tokens*) * tokens_length);
		}
		struct token* token = (struct token*) malloc(sizeof(struct token));
		token->type = token_type;
		token->string = strndup(token_string, token_length);
		tokens[tokens_used++] = token;
	}

	*tokens_ptr = tokens;
	*tokens_used_ptr = tokens_used;
	return true;
}
