#ifndef TOKENIZE_H
#define TOKENIZE_H

#include <stdbool.h>
#include <stddef.h>

struct token;

bool tokenize(struct token*** tokens_ptr,
              size_t* tokens_used_ptr,
              const char* input);

#endif
