#ifndef PARSE_H
#define PARSE_H

#include <stdbool.h>
#include <stddef.h>

struct ast;
struct token;

struct ast* parse(struct token** tokens, size_t tokens_length);

#endif
