#include <errno.h>
#include <error.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "token.h"
#include "ast.h"

#include "input.h"
#include "tokenize.h"
#include "parse.h"
#include "check.h"
#include "codegen.h"

int main(int argc, char* argv[])
{
	const char* path;
	char* input;
	if ( argc <= 1 )
	{
		path = "<stdin>";
		input = read_stream(stdin);
	}
	else
	{
		path = argv[1];
		FILE* fp = fopen(path, "r");
		if ( !fp )
			error(1, errno, "%s", path);
		input = read_stream(fp);
		fclose(fp);
	}
	struct token** tokens;
	size_t tokens_length;
	if ( !tokenize(&tokens, &tokens_length, input) )
		return 1;
	free(input);
#if 0
	for ( size_t i = 0; i < tokens_length; i++ )
	{
		struct token* token = tokens[i];
		printf("%zu. %s ‘%s’\n", i, string_of_token_type(token->type), token->string);
	}
#endif
	struct ast* ast;
	if ( !(ast = parse(tokens, tokens_length)) )
		return 1;
#if 0
	ast_print(stdout, ast);
#endif
	if ( !(check(ast)) )
		return 1;
#if 0
	ast_print(stdout, ast);
#endif
	if ( !codegen(stdout, ast, path) )
		return 1;
	return 0;
}
