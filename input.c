#include <stdio.h>
#include <stdlib.h>

#include "input.h"

char* read_stream(FILE* fp)
{
	size_t buffer_used = 0;
	size_t buffer_length = 0;
	char* buffer = (char*) malloc(buffer_length + 1);
	int ic;
	while ( (ic = fgetc(fp)) != EOF )
	{
		if ( buffer_used == buffer_length )
		{
			buffer_length = buffer_length ? 2 * buffer_used : 16;
			buffer = (char*) realloc(buffer, buffer_length + 1);
		}
		buffer[buffer_used++] = (char) ic;
	}
	buffer[buffer_used] = '\0';
	return buffer;
}
