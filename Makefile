CC=gcc
CFLAGS=-O2 -g -Wall -Wextra

OBJS=\
ast.o \
cc.o \
check.o \
codegen.o \
input.o \
parse.o \
tokenize.o \

cc: $(OBJS)
	$(CC) -std=gnu11 $(OBJS) -o $@ $(CFLAGS)

.SUFFIXES: .c .o

.c.o:
	$(CC) -std=gnu11 -c $< -o $@ $(CFLAGS)

clean:
	rm -f $(OBJS) *.o
	rm -f cc
