#ifndef AST_H
#define AST_H

#include <sys/types.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

struct ast_type;

enum ast_type_basic
{
	AST_TYPE_BASIC_INT,
};

struct ast_type_function
{
	struct ast_type* return_type;
	struct ast_type** param_types;
	size_t param_types_length;
};

enum ast_type_type
{
	AST_TYPE_BASIC,
	AST_TYPE_FUNCTION,
};

struct ast_type
{
	enum ast_type_type type;
	union
	{
		enum ast_type_basic basic;
		struct ast_type_function function;
	};
};

struct ast_type* ast_type_clone(struct ast_type* type);
void ast_type_free(struct ast_type* type);

struct ast_expression;

struct ast_constant_integer
{
	char* string;
};

struct ast_identifier
{
	char* string;
};

enum ast_binop_type
{
	AST_BINOP_ASSIGNMENT,
	AST_BINOP_PLUS,
	AST_BINOP_MINUS,
	AST_BINOP_MULTIPLY,
	AST_BINOP_DIVIDE,
};

struct ast_binop
{
	enum ast_binop_type type;
	struct ast_expression* lhs;
	struct ast_expression* rhs;
};

struct ast_function_call
{
	struct ast_expression* func_expr;
	struct ast_expression** param_exprs;
	size_t param_exprs_length;
};

struct ast_expression_local
{
	struct ast_statement_variable* var; /* non-owning */
};

struct ast_expression_parameter
{
	struct ast_parameter* param; /* non-owning */
};

struct ast_expression_global
{
	struct ast_global_variable* var; /* non-owning */
};

struct ast_expression_function
{
	struct ast_function* func; /* non-owning */
};

enum ast_expression_type
{
	AST_EXPRESSION_CONSTANT_INTEGER,
	AST_EXPRESSION_IDENTIFIER,  /* removed in phase check */
	AST_EXPRESSION_BINOP,
	AST_EXPRESSION_FUNCTION_CALL,
	AST_EXPRESSION_LOCAL, /* added in phase check */
	AST_EXPRESSION_PARAMETER, /* added in phase check */
	AST_EXPRESSION_GLOBAL, /* added in phase check */
	AST_EXPRESSION_FUNCTION, /* added in phase check */
};

struct ast_expression
{
	enum ast_expression_type type;
	struct ast_type* expression_type; /* set in phase check */
	bool is_lvalue; /* set in phase check */
	union
	{
		struct ast_constant_integer constant_integer;
		struct ast_identifier identifier; /* removed in phase check */
		struct ast_binop binop;
		struct ast_function_call function_call;
		struct ast_expression_local local; /* added in phase check */
		struct ast_expression_parameter parameter; /* added in phase check */
		struct ast_expression_global global; /* added in phase check */
		struct ast_expression_function function; /* added in phase check */
	};
};

struct ast_statement;

struct ast_statement_block
{
	struct ast_statement** stms;
	size_t stms_length;
};

struct ast_statement_expression
{
	struct ast_expression* expr;
};

struct ast_statement_return
{
	struct ast_expression* expr;
};

struct ast_statement_variable
{
	struct ast_type* type;
	char* name;
	ssize_t frame_offset; /* set in phase codegen */
};

enum ast_statement_type
{
	AST_STATEMENT_BLOCK,
	AST_STATEMENT_EXPRESSION,
	AST_STATEMENT_RETURN,
	AST_STATEMENT_VARIABLE,
};

struct ast_statement
{
	enum ast_statement_type type;
	union
	{
		struct ast_statement_block block;
		struct ast_statement_expression expression;
		struct ast_statement_return return_stm;
		struct ast_statement_variable variable;
	};
};

struct ast_parameter
{
	struct ast_type* type;
	char* name;
	ssize_t frame_offset; /* set in phase codegen */
};

struct ast_function
{
	struct ast_type* return_type;
	char* name;
	struct ast_statement* body;
	struct ast_parameter** params;
	size_t params_length;
};

struct ast_global_variable
{
	struct ast_type* type;
	char* name;
};

struct ast_toplevel;

struct ast_toplevel_function
{
	struct ast_function* func;
};

struct ast_toplevel_variable
{
	struct ast_global_variable* var;
};

enum ast_toplevel_type
{
	AST_TOPLEVEL_FUNCTION,
	AST_TOPLEVEL_VARIABLE,
};

struct ast_toplevel
{
	enum ast_toplevel_type type;
	union
	{
		struct ast_toplevel_function function;
		struct ast_toplevel_variable variable;
	};
};

struct ast
{
	struct ast_toplevel** tlvls;
	size_t tlvls_length;
};

void ast_print(FILE* fp, struct ast* ast);

#endif
