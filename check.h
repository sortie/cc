#ifndef CHECK_H
#define CHECK_H

#include <stdbool.h>

struct ast;

bool check(struct ast* ast);

#endif
